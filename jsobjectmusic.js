// OBJECT EXERCISE MUSIC PLAYER


function Player() {
    this.currentSong = 0;
    this.tracks = [];
}

Player.prototype.add = function(track){
    this.tracks.push(track);
}

Player.prototype.play = function(track){
    console.log(this.tracks[this.currentSong].title + ' by ' + this.tracks[this.currentSong].artist);
}
Player.prototype.next = function(track){
    if(this.currentSong + 1 > this.tracks.length-1){
        this.currentSong = 0;
    } else {
        this.currentSong += 1;
    }
}
Player.prototype.previous = function(track){
    if(this.currentSong - 1 < 0){
        this.currentSong = this.tracks.length-1;
    } else {
        this.currentSong -= 1;
    }
}

function Track(artist, title, album) {
    this.artist = artist;
    this.title = title;
    this.album = album;
}
Track.prototype.play = function(){
    console.log(this.title + ' by ' + this.artist);
}
var driveTrack = new Track('Incubus', 'Drive', 'Make Yourself');
var laBambaTrack = new Track('Ritchie Valens', 'La Bamba', 'La Bamba');

laBambaTrack.play()


var player = new Player();
player.add(driveTrack);
player.add(laBambaTrack);
player.play();
player.next();
player.play();
player.next();
player.play();
player.next();
player.play();
player.next();
player.play();
player.next();
player.play();
player.next();
player.play();

player.previous();
player.play();
player.previous();
player.play();
player.previous();
player.play();
player.previous();
player.play();
player.previous();
player.play();
player.previous();
player.play();